# duetai



## Getting started

Please run this script from the Cloud Shell in a GCP Project.

This script enables DuetAI in a GCP Project via enabling the Cloud AI Companion API and adds the specified principal (user's email) to the project's IAM policy with the Cloud AI Companion User and Service Usage Viewer roles.

Please note that this is NOT a Google Cloud supported code but just a wrapper written on top of commands outlined in this link (https://cloud.google.com/duet-ai/docs/discover/set-up-duet-ai#activate-duet-ai)

Usage:

**Enable DuetAI for Single Project and Single User**

bash duetai-enable-single-user.sh

**Enable DuetAI for Single Project and Multiple Users**

bash duet-enable-multi-user.sh


You can clone with ssh using git@gitlab.com:nakulgandha/duetai.git 

You can clone with https using https://gitlab.com/nakulgandha/duetai.git 
