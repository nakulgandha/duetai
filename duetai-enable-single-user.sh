#!/bin/bash


# This script enables DuetAI in a GCP Project via enabling the Cloud AI Companion API and adds the specified principal to the project's IAM policy with the Cloud AI Companion User and Service Usage Viewer roles.

# Get the project ID from the user.
echo "Enter your project ID:"
read PROJECT_ID

# Get the principal from the user.
echo "Enter the user's email to add to the project's IAM policy to enable DuetAI:"
read PRINCIPAL

# Enable the Cloud AI Companion API.
gcloud services enable cloudaicompanion.googleapis.com --project $PROJECT_ID

# Add the principal to the project's IAM policy with the Cloud AI Companion User role.
gcloud projects add-iam-policy-binding $PROJECT_ID --member=user:$PRINCIPAL --role=roles/cloudaicompanion.user

# Add the principal to the project's IAM policy with the Service Usage Viewer role.
gcloud projects add-iam-policy-binding $PROJECT_ID --member=user:$PRINCIPAL --role=roles/serviceusage.serviceUsageViewer
