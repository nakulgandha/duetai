#!/bin/bash


# This script enables DuetAI in a GCP Project for multiple users via enabling the Cloud AI Companion API and adds the specified principals to the project's IAM policy with the Cloud AI Companion User and Service Usage Viewer roles. Please note that it is not tested at scale and is not s google cloud supported script. However, it works and simplifies the job.

# Get the project ID from the user.
echo "Enter your project ID:"
read PROJECT_ID

# Get the list of principals from the user.
echo "Enter the list of principals (user's email id) to add to the project's IAM policy to enable DuetAI (space-separated):"
read PRINCIPALS

# Enable the Cloud AI Companion API.
gcloud services enable cloudaicompanion.googleapis.com --project $PROJECT_ID

# Add each principal to the project's IAM policy with the Cloud AI Companion User role.
for PRINCIPAL in $PRINCIPALS; do
  gcloud projects add-iam-policy-binding $PROJECT_ID --member=user:$PRINCIPAL --role=roles/cloudaicompanion.user
done

# Add each principal to the project's IAM policy with the Service Usage Viewer role.
for PRINCIPAL in $PRINCIPALS; do
  gcloud projects add-iam-policy-binding $PROJECT_ID --member=user:$PRINCIPAL --role=roles/serviceusage.serviceUsageViewer
done
